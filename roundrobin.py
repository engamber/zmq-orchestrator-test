import threading
import random
import time
import zmq

NTRANSMITTERS = 10
NRECEIVERS = 10
ITERATIONS = 1000

def transmitter(id):
  context = zmq.Context()
  # subscribe socket for random events
  sub = context.socket(zmq.SUB)
  sub.connect("tcp://localhost:5755")
  sub.subscribe(b'')
  
  # pair sockets to receivers
  pairs = [None] * NRECEIVERS
  for i in range(0, NRECEIVERS):
    pairs[i] = context.socket(zmq.PAIR)
    port = 5760+(i*NTRANSMITTERS)+id
    pairs[i].connect("tcp://localhost:%s" % str(port))
    # print("tx", id, "rx", i, port)
 
  # allow time for setup
  time.sleep(0.1)

  # round-robin for pair sockets
  rr = 0
  while(True):
    # receive random event
    obj = sub.recv_pyobj()
    # print("transmitter", id, rr, obj)
    pairs[rr].send_pyobj(obj)
    rr = rr + 1
    if rr == NRECEIVERS:
      rr = 0
    
def receiver(id):
  context = zmq.Context()
  # pair sockets to transmitters
  pairs = [None] * NTRANSMITTERS
  for i in range(0, NTRANSMITTERS):
    pairs[i] = context.socket(zmq.PAIR)
    port = 5760+(id*NTRANSMITTERS)+i
    pairs[i].bind("tcp://*:%s" % str(port))
    # print("rx", id, "tx", i, port)
  
  # allow time for setup
  time.sleep(0.1)

  # counter for total sleep
  total = 0.
  while(True):
    # receive object from all pairs (they will be the same)
    for p in pairs:
      obj = p.recv_pyobj()
      # print("receiver", id, obj)
    if obj == "DONE":
      # print total work time
      print("receiver", id, "done", total)
      return
    else:
      time.sleep(obj)
      total = total + obj

  
# main
context = zmq.Context()

# publisher socket for random events
pub = context.socket(zmq.PUB)
pub.bind("tcp://*:5755")

transmitter_threads = []
for i in range(0, NTRANSMITTERS):
  transmitter_threads.append(threading.Thread(target=transmitter, args=(i,)))
for t in transmitter_threads:
  t.start()

receiver_threads = []
for i in range(0, NRECEIVERS):
  receiver_threads.append(threading.Thread(target=receiver, args=(i,)))
for t in receiver_threads:
  t.start()

time.sleep(0.2)

# random event generator (event is a float)
random.seed(0)

ts = time.time()
# send [iterations] random numbers to sender_threads
for _ in range(0, ITERATIONS):
  # time.sleep(0.1)
  num = random.expovariate(2)
  pub.send_pyobj(num)
for i in range(0, NRECEIVERS):
  done = "DONE"
  pub.send_pyobj(done)


for t in receiver_threads:
  t.join()
te = time.time()
print("took", te-ts)
