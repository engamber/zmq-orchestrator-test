# ZMQ Orchestrator Test

### Parameters

`NTRANSMITTERS`, `NRECEIVERS`, `ITERATIONS` can be modified at the head of the script.

### Running

Just execute one of the two stand-alone self-contained scripts with:

    python3
